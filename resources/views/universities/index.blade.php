@extends('layouts.app')

@section('content')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Universidades</h1>    
  <table class="table table-striped">
    <thead>
        <a href=" {{ route('universities.create') }}" class="btn btn-primary" >Agregar</a>
        @if(session()->get('success'))
                    <div class="alert alert-success">
                     {{ session()->get('success') }}  
                    </div>
                @endif
        <tr>
          <td>Nombre</td>
          <td>Link</td>          
          <td colspan = 2>Acciones</td>
        </tr>
    </thead>
    <tbody>
        @foreach($universities as $university)
        <tr>
            <td><a href=" {{ route('universities.show', $university->id) }}"> {{ $university->name }} </a></td>
            <td>{{$university->link}}</td>     
            <td> 
                <a href="{{ route('universities.edit',$university->id)}}" class="btn btn-primary">Editar</a>
            </td>
            <td>
                <form action="{{ route('universities.destroy', $university->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Borrar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
@endsection
