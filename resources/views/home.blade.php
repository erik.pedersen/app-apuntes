@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mb-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center mb-4">  
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Publicar nuevo mensaje!</h5>
                        <p class="card-text">¿Tenés material para compartir? Compartilo acá</p>
                        <a href="{{ route('posts.create') }}" class="btn btn-primary">Compartir</a>
                    </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Más leídos</h5>
                        <p class="card-text">Mira los aportes mas vistos del momento.</p>
                        <a href="{{ route('posts.index') }}" class="btn btn-primary">Ver más leídos</a>
                    </div>
                </div>
            </div>
                 
        </div>            
    </div>       
</div>
@endsection
