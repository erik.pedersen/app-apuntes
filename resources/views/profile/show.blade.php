@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center mb-4">
    <div class="col-md-8">
      <h2>{{ Auth::user()->name }}</h2>
      <h3>Listado de publicaciones</h3>
      <ul>
      @foreach($posts as $post)      
      <li><a href="{{ route('posts.show', $post->id) }}">{{ $post->title }}</a></li>
      @endforeach
      </ul>
    </div>
  </div>
</div>

@endsection


