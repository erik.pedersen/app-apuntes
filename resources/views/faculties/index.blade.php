@extends('layouts.app')

@section('content')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Facultades</h1>    
  <table class="table table-striped">
    <thead>
        <a href=" {{ route('faculties.create') }}" class="btn btn-primary" >Agregar</a>
        @if(session()->get('success'))
                    <div class="alert alert-success">
                     {{ session()->get('success') }}  
                    </div>
                @endif
        <tr>
          <td>Nombre</td>
          <td colspan = 4>Universidad</td>          
        </tr>
    </thead>
    <tbody>
        @foreach($faculties as $faculty)
        <tr>
            <td><a href=" {{ route('faculties.show', $faculty->id) }}"> {{ $faculty->name }} </a></td>
            <td>{{ $faculty->university_id }}</td>     
            <td> 
                <a href="{{ route('faculties.edit',$faculty->id)}}" class="btn btn-primary">Editar</a>
            </td> 
            <td>
                <form action="{{ route('faculties.destroy', $faculty->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Borrar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
@endsection
