@extends('layouts.app')

@section('content')
<div class="container">
<div class="row justify-content-center mb-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Editar facultad</div>

                <div class="card-body">
                  <form class="" method="post" action="{{ route('faculties.update', $faculty->id) }}">
                  @method('PATCH')
                  @csrf
                  @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
                  <br />
                  @endif

                  <div class="form-group">
                    <label for="faculty">Faculty</label>
                    <input type="text" class="form-control" name="name" value="{{ $faculty->name }}"/>
                  </div>

                  <div class="form-group">
                    <label for="university_id">Universidad:</label>
                    
                    <select type="text" class="form-control" name="university_id" id="">
                      <option disabled>Seleccione una universidad...</option>
                      @foreach($universities as $university)
                        <option value="{{ $university->id }}" {{ $university->id == $faculty->university_id ? 'selected' : '' }}>{{ $university->name }}</option>
                      @endforeach
                    </select>
                  </div>

                  <button type="submit" class="btn btn-primary">Actualizar facultad</button>
                </form>
              </div>
            </div>
        </div>
    </div>
</div>

@endsection
