@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Detalle de la facultad</div>               
                @if(session()->get('success'))
                    <div class="alert alert-success">
                     {{ session()->get('success') }}  
                    </div>
                @endif
                <div class="card-body">
                <ul>
                    <li>Nombre: {{ $faculty->name }}</li>
                    <li>Universidad: {{ $faculty->university_id}}</li>               
                </ul>   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
