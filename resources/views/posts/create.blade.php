@extends('layouts.app')

@section('content')
<div class="container">
<div class="row justify-content-center mb-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Nuevo mensaje</div>

                <div class="card-body">
                  <form class="" method="post" action="{{ route('posts.store') }}" enctype="multipart/form-data">  
                  @csrf
                  @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
                  <br /> 
                  @endif

                  <div class="form-group">
                    <label class="my-1 mr-2" for="subject_id">Materia</label>
                    <select class="form-control custom-select my-1 mr-sm-2" name="subject_id">
                        <option selected disabled>Seleccionar materia...</option>
                        @foreach ($subjects as $subject)
                        <option value="{{ $subject->id }}">{{ $subject->name }}</option>                        
                        @endforeach
                    </select>
                  </div>
                  
                  <div class="form-group">
                    <label for="title">Titulo:</label>
                    <input type="text" class="form-control" name="title" value="" />
                  </div>

                  <div class="form-group">
                    <label for="content">Mensaje:</label>
                    <textarea class="form-control" name="content" rows="3"></textarea>
                  </div>

                  <div class="form-group">
                    <label for="attachment_url">Adjunto:</label>
                    <input type="file" id="imagesToUpload" name="imagesToUpload[]" multiple>
                  </div>

                  <button type="submit" class="btn btn-primary">Publicar comentario</button>
                </form>
              </div>
            </div>
        </div>
    </div>
</div>

@endsection


