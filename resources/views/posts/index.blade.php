@extends('layouts.app')

@section('content')
<div class="container">
<div class="row justify-content-center mb-4">

        <div class="col-md-8">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <div class="card mb-4">
              <div class="card-body">
                  <h5 class="card-title">Publicar nuevo mensaje!</h5>
                  <p class="card-text">¿Tenés material para compartir? Compartilo acá</p>
                  <a href="{{ route('posts.create') }}" class="btn btn-primary">Compartir</a>
              </div>
            </div>
            <div class="card">
                <div class="card-header">Ultimas publicaciones</div>

                <div class="card-body">
                  <div class="card-deck">
                  @foreach ($posts as $post)
                    <div class="card">           
                      @if ( !empty($post->attachment_url) ) 
                      <img class="card-img-top" src="{{ Storage::url($post->attachment_url) }}" alt="{{ $post->title }}">
                      @endif
                      <div class="card-body">
                        <h5 class="card-title">{{ $post->title }}</h5>
                        <p class="card-text text-right"><small class="text-muted">Creado {{ $post->created_at->ago() }}</small></p>                        
                        <p class="card-text">{{ Str::limit($post->content,'50', '...') }}</p>
                        <a href="{{ route('posts.show',$post->id) }}" class="card-link">Ver publicación</a>
                        <p class="card-text"><small class="text-muted">Por {{ $post->user->name }} en {{ $post->subject->name }}</small></p>
                        @can('update',$post)
                          <a href="{{ route('posts.edit',$post->id) }}" class="btn btn-primary">Editar</a>
                        @endcan
                      </div>                      
                    </div>
                  @endforeach
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


