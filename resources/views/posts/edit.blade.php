@extends('layouts.app')

@section('content')
<div class="container">
<div class="row justify-content-center mb-4">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Editar publicación</div>

                <div class="card-body">
                  <form class="" method="post" action="{{ route('posts.update', $post->id) }}">  
                  @method('PATCH') 
                  @csrf
                  @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
                  <br /> 
                  @endif

                  <div class="form-group">
                    <label class="my-1 mr-2" for="subject_id">Materia</label>
                    <select class="form-control custom-select my-1 mr-sm-2" name="subject_id">
                        <option disabled>Seleccionar materia...</option>
                        @foreach ($subjects as $subject)
                        <option selected="{{ $post->subject_id == $subject->id }}" value="{{ $subject->id }}">{{ $subject->name }}</option>                        
                        @endforeach
                    </select>
                  </div>
                  
                  <div class="form-group">
                    <label for="title">Titulo:</label>
                    <input type="text" class="form-control" name="title" value="{{ $post->title }}" />
                  </div>

                  <div class="form-group">
                    <label for="content">Mensaje:</label>
                    <textarea class="form-control" name="content" rows="3" >{{ $post->content }}</textarea>
                  </div>

                  <div class="form-group">
                    <label for="attachment_url">Adjunto (URL):</label>
                    <input type="text" class="form-control" name="attachment_url" value="{{ $post->attachment_url }}" />
                  </div>

                  <button type="submit" class="btn btn-primary">Actualizar publicación</button>
                </form>
              </div>
            </div>
        </div>
    </div>
</div>

@endsection


