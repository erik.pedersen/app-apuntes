<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function () {
    
    /* Resources */
    Route::resource('faculties', 'FacultyController');
    Route::resource('posts', 'PostController');
    Route::resource('universities', 'UniversityController');
    
    /* Custom */
    Route::get('profile', 'ProfileController@show');
});

