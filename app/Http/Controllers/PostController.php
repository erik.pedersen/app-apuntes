<?php

namespace App\Http\Controllers;

use App\Post;
use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{

    public function __construct()
    {     
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with('user')
            ->with('subject')
            ->orderBy('created_at', 'desc')
            ->paginate(15);
        return view('posts.index', [
            'posts' => $posts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subjects = Subject::select('id','name')->get();
        return view('posts.create', [
            'subjects' => $subjects
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required',
            'content'=>'required',
            'subject_id' =>'required',
        ]);

        $path = Storage::putFile('public/post_files',$request->file('attachment'));

        $post = new Post([
            'title' => $request->get('title'),
            'content' => $request->get('content'),
            'user_id' => Auth::user()->id,
            'subject_id' => $request->get('subject_id'),    
            'attachment_url' => $path,                    
        ]);
        $post->save();
        return redirect('/posts')->with('success', '¡Mensaje creado con éxito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $this->authorize('update', $post);
        
        $subjects = Subject::select('id','name')->get();
        return view('posts.edit', [
            'subjects' => $subjects,
            'post' => $post
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $this->authorize('update', $post);
        
        $request->validate([
            'title'=>'required',
            'content'=>'required',
            'subject_id'=>'required'
        ]);

        $post->title =  $request->get('title');
        $post->content = $request->get('content');
        $post->subject_id = $request->get('subject_id');
        $post->save();

        return redirect('/posts')->with('success', '¡Publicacion actualizada!');
         
        
        //return redirect('/posts')->with('error', 'No tiene permisos');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
