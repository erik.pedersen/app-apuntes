<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    //

    public function show()
    {
        $posts = Post::where('user_id',Auth::id())->get();
        return view('profile.show', [
            'posts' => $posts
        ]);
    }
}
