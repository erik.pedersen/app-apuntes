<?php

namespace App\Http\Controllers;

use App\Faculty;
use App\University;
use Illuminate\Http\Request;

class FacultyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faculties = Faculty::all();
        return view('faculties.index', [
            'faculties' => $faculties
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $universities = University::select('id', 'name')->get();
        return view('faculties.create', [
            'universities' => $universities,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'university_id'=>'required',
        ]);
        
        $faculty = new Faculty([
            'name' => $request->get('name'),
            'university_id' => $request->get('university_id'),
        ]);
        $faculty->save();
        return redirect('/faculties')->with('success', 'Facultad cargada con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function show(Faculty $faculty)
    {
        return view ('faculties.show', ['faculty'=>$faculty]); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function edit(Faculty $faculty)
    {
        $universities = University::select('id', 'name')->get();
        return view('faculties.edit', [
            'universities' => $universities,
            'faculty' => $faculty
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faculty $faculty)
    {
         $request->validate([
            'name'=>'required',
            'university_id'=>'required'
        ]);

        $faculty->name = $request->get('name');
        $faculty->university_id = $request->get('university_id');
        $faculty->save();
        return redirect('/faculties')->with('success', '¡Facultad actualizada con éxito!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Faculty  $faculty
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faculty $faculty)
    {
        $faculty->delete();
        return redirect('/faculties')->with('success', '¡Facultad borrada con éxito!');

    }
}
