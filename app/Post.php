<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'content', 'subject_id', 'user_id'];

    public function subject(){
        return $this->belongsTo('App\Subject');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function getIsRecentAttribute(){
        return \Carbon\Carbon::now()->diffInDays($this->created_at) <= 1;
    }

    public function imagenes(){
        return $this->hasMany('App\Media');
    }
}
