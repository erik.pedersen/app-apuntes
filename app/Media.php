<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
   protected $fillable = ['id', 'path'];
   protected $table = 'media';
   public function media(){
       return $this->belongsTo('App\Post');
   }
}
