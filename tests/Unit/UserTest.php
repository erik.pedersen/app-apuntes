<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testFullName()
    {
        $user = factory(\App\User::class)->create([
                'name' => 'Cacho',
                'lastname' => 'De Buenos Aires'
            ]);
        $this->assertEquals('Cacho De Buenos Aires', $user->fullName);
    }
}
