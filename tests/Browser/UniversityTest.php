<?php

namespace Tests\Browser;

use App\User;
use App\University;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UniversityCRUDTest extends DuskTestCase
{
    use DatabaseMigrations;
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testShowUniversities()
    {
        $user = factory(User::class)->create();
        $university = factory(University::class)->create();

        $this->browse(function (Browser $browser) use ($user, $university) {
         $browser->loginAs($user)
                 ->visit('/universities')
                 ->assertSee($university->name)
                 ->logout();
        });
    }

    public function testShowCreateUniversity()
    {
        $user = factory(User::class)->create();        

        $this->browse(function (Browser $browser) use ($user) {
         $browser->loginAs($user)
                 ->visit('/universities/create')
                 ->assertVisible('input[name="name"]')
                 ->assertVisible('input[name="link"]')
                 ->logout();
        });
    }

    public function testShowUniversityDetails()
    {
        $user = factory(User::class)->create();
        $university = factory(University::class)->create();

        $this->browse(function (Browser $browser) use ($user, $university) {
         $browser->loginAs($user)
                 ->visit(route('universities.show', $university->id))
                 ->assertSee($university->name)
                 ->logout();
        });
    }    

    public function testShowEditUniversity()
    {
        $user = factory(User::class)->create();
        $university = factory(University::class)->create();

        $this->browse(function (Browser $browser) use ($user, $university) {
         $browser->loginAs($user)
                 ->visit(route('universities.edit', $university->id))
                 ->assertValue('input[name="name"]',$university->name)
                 ->logout();
        });
    }

    public function testEditUniversity()
    {
        $user = factory(User::class)->create();
        $university = factory(University::class)->create();

        $this->browse(function (Browser $browser) use ($user, $university) {
            $newName = 'test';
            $url = 'http';
            $browser->loginAs($user)
                    ->visit(route('universities.edit', $university->id))
                    ->type('name', $newName)
                    ->type('link',$url)
                    ->press('Actualizar universidad')
                    ->assertSee($newName)
                    ->logout();
        });
    }

    public function testDeleteUniversity()
    {
        $user = factory(User::class)->create();
        $university = factory(University::class)->create();

        $this->browse(function (Browser $browser) use ($user, $university) {            
            $browser->loginAs($user)
                    ->visit(route('universities.index'))
                    ->press('Borrar')
                    ->assertSee('Universidades')
                    ->logout();
        });
    }

    public function testUpdateUniversity()
    {
        $user = factory(User::class)->create();
        $university = factory(University::class)->create();

        $this->browse(function (Browser $browser) use ($user, $university) {
            $newName = 'test';
            $url = 'http';
            $browser->loginAs($user)
                    ->visit(route('universities.edit', $university->id))
                    ->type('name', $newName)
                    ->type('link',$url)
                    ->press('Actualizar universidad')
                    ->visit(route('universities.show', $university->id))
                    ->assertSee($newName)
                    ->assertSee($url)
                    ->logout();
        });
    }
}