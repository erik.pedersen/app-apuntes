<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    use DatabaseMigrations;
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testLoginSuccessful()
    {
        $user = factory(User::class)->create([
            'password' => bcrypt('12345678')
        ] );

        $this->browse(function (Browser $browser) use ($user) {       
            $browser->visit('/login') 
                    ->type('email', $user->email)
                    ->type('password','12345678')
                    ->press('Login')
                    ->assertSee('Dashboard')
                    ->logout();
        });
    }

    public function testFailed()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create([
                'password' => bcrypt('12345678')
            ] );

            $browser->visit('/login')
                    ->type('email', $user->email)
                    ->type('password','123456789')
                    ->press('Login')
                    ->waitForText('do not match')
                    ->assertSee('do not match');
        });
    }
    
    public function test3Failed()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create([
                'password' => bcrypt('12345678')
            ] );

            $browser->visit('/login')
                    ->type('email', $user->email)
                    ->type('password','not123456789')
                    ->press('Login')
                    ->waitForText('do not match')
                    ->assertSee('do not match')
                    ->visit('/login')
                    ->type('email', $user->email)
                    ->type('password','not123456789')
                    ->press('Login')
                    ->waitForText('do not match')
                    ->assertSee('do not match')
                    ->visit('/login')
                    ->type('email', $user->email)
                    ->type('password','not123456789')
                    ->press('Login')
                    ->waitForText('do not match')
                    ->assertSee('do not match')
                    ->logout();                   
        });
    }
}