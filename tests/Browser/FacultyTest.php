<?php

namespace Tests\Browser;

use App\User;
use App\Faculty;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class FacultyCRUDTest extends DuskTestCase
{
    use DatabaseMigrations;
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testShowFaculties()
    {
        $user = factory(User::class)->create();
        $faculty = factory(Faculty::class)->create();

        $this->browse(function (Browser $browser) use ($user, $faculty) {
         $browser->loginAs($user)
                 ->visit('/faculties')
                 ->assertSee($faculty->name)
                 ->logout();
        });
    }

    public function testShowCreateFaculty()
    {
        $user = factory(User::class)->create();        

        $this->browse(function (Browser $browser) use ($user) {
         $browser->loginAs($user)
                 ->visit('/faculties/create')
                 ->assertVisible('input[name="name"]')
                 ->assertVisible('select[name="university_id"]')
                 ->logout();
        });
    }

    public function testShowFacultyDetails()
    {
        $user = factory(User::class)->create();
        $faculty = factory(Faculty::class)->create();

        $this->browse(function (Browser $browser) use ($user, $faculty) {
         $browser->loginAs($user)
                 ->visit(route('faculties.show', $faculty->id))
                 ->assertSee($faculty->name)
                 ->logout();
        });
    }    

    public function testShowEditFaculty()
    {
        $user = factory(User::class)->create();
        $faculty = factory(Faculty::class)->create();

        $this->browse(function (Browser $browser) use ($user, $faculty) {
         $browser->loginAs($user)
                 ->visit(route('faculties.edit', $faculty->id))
                 ->assertValue('input[name="name"]',$faculty->name)
                 ->logout();
        });
    }

    public function testEditFaculty()
    {
        $user = factory(User::class)->create();
        $faculty = factory(Faculty::class)->create();

        $this->browse(function (Browser $browser) use ($user, $faculty) {
            $newName = 'test';
            $browser->loginAs($user)
                    ->visit(route('faculties.edit', $faculty->id))
                    ->type('name', $newName)
                    ->select('university_id')
                    ->press('Actualizar facultad')
                    ->assertSee($newName)
                    ->logout();
        });
    }

    public function testDeleteFaculty()
    {
        $user = factory(User::class)->create();
        $faculty = factory(Faculty::class)->create();

        $this->browse(function (Browser $browser) use ($user, $faculty) {            
            $browser->loginAs($user)
                    ->visit(route('faculties.index'))
                    ->press('Borrar')
                    ->assertSee('Facultades')
                    ->logout();
        });
    }

}
