<?php

use Illuminate\Database\Seeder;

class SubjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faculty = \App\Faculty::first();
        \App\Subject::create([
            'name' => 'Programación Web 2',
            'faculty_id' => $faculty->id
        ]);
    }
}
