<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use App\Faculty;
use App\University;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Faculty::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'university_id' => University::create(['name' => 'unicen'])->id,
    ];
});
